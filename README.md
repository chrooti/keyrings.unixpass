# keyring.unixpass

This plugin for the keyring library adds support for pass, the standard unix passowrd manager.

## Usage

```bash
pip install keyring keyrings.unixpass
```

## Development

The `./tasks.sh` file should be self explanatory.
After running the tests you might want to `pass git reset --hard $branch` if you're using the git features of pass.
