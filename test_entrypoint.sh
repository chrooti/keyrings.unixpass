#!/bin/sh

set -e

apt-get -y update
apt-get -y install pass

cd /app || exit 0
gpg --batch --passphrase '' --quick-gen-key TestKey default default
pass init TestKey

python -m venv .test_venv
# shellcheck disable=SC1091
. .test_venv/bin/activate

pip install -U pip
pip install keyring
python -m unittest tests
