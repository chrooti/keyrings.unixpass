import unittest
import uuid

import keyrings.unixpass


class KeyringsPassTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        service_uuid = uuid.uuid4()
        cls.service = f"keyrings.unixpass/test/{service_uuid}"

        cls.username = uuid.uuid4()

    def setUp(self):
        self.k = keyrings.unixpass.Keyring()

    def test_init_keyring(self):
        self.assertTrue(self.k.viable)

    def test_keyring_works(self):
        self.k.set_password(self.service, self.username, "password")

        password = self.k.get_password(self.service, self.username)
        self.assertEqual(password, "password")

        self.k.delete_password(self.service, self.username)

        password_after_delete = self.k.get_password(self.service, self.username)
        self.assertIsNone(password_after_delete)
